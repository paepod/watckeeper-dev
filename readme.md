Watchkeeper
====================
Administration (Done)
---------------------
- Manage Countries 
    - Add new Country
    - Update exiting Country
    - Active or Inactive Country
- Manage Classification 
    - Add new Classification
    - Update exiting classification
    - Active or Inactive
- Manage Users 
    - Add new user
    - Edit user (Firstname,Middlename and Lastname)
    - Assign countries to users
    - Assign roles to users
    - Active or Inactive
    - Activate User by Email after created
    - Automate password generatid after created user

- Manage Permissions (new requreiement) 
    - Add new permissions
    - Update exiting permissions
    - Delete exiting permissions
    - Group permission base on Super-Admin, Admin, Super-User, User
    - Admin authorization
- Manage Role (new requreiement) 
    - Add new role
    - Update exiting role
    - Assign Permission to Role

- Manage Point and Areas (new requreiement)    
- Add new point and areas
    - Update point and areas
    - Show map base on computer location
    - Search map location like google map search
    - Multiple marker on the map
    - Multiple zone on the map
    - Allow one style marker or zone on the map
    - GEOJson

Security Management (Done)
---------------------
- Incident Alert (Simple Form) 
    - Add new Incident
    - Update Exiting Incident
    - Delete Incident
    - Add Location base marker or zone
    - Automatic Zoom to user location
    - Specification Location base on Point and Areas or Manual
    - Location searching like goole map (in Location textbox)
- Point of Incident (POI) 
    - Add new POI
    - Update Exiting POI
    - Delete POI
    - Upload picture
    - Add Location base marker or zone
    - Automatic Zoom to user location
    - Default distance is 20 km
    - Specification Location base on Point and Areas or Manual
    - Location searching like goole map (in Location textbox)
- Security Advisory 
    - Add new security advisory
    - Update Exiting security advisory
    - Delete security advisory
    - Add Location base marker or zone
    - Automatic Zoom to user location
    - Specification Location base on Point and Areas or Manual
    - Location searching like goole map (in Location textbox)
- Threat Event 
    - Add new threat
    - Update Exiting threat
    - Delete threat
    - Add Location base marker or zone
    - Automatic Zoom to user location
    - Specification Location base on Point and Areas or Manual
    - Location searching like goole map (in Location textbox)
    - Send Email threat alert after added threat base on user country
- Risk Level and Movement 
    - Add new movement
    - Update Exiting movement
    - Delete movement
    - Add Location base marker or zone
    - Automatic Zoom to user location
    - Specification Location base on Point and Areas or Manual
    - Location searching like goole map (in Location textbox)

On Going
---------------------
- Home page [Link](http://playground.oasiswebservice.org/security)
    - Login
    - Change Password
    - Current Thread
    - Current Security Advisory
    - Current Risk
    - Security Map
    - Contact form
    - ISLS
    - History
    - All Report Incident
    - Disclimer

Pending
---------------------
- __Location searching on the map on all Security Management__
- __Incident Enhance Form__
- __Send Email and SMS Alert base on Point of incident (need more infomation) __
- __Send and Resend SMS Thread Alert  base on country thread occurs__
- __Resend Email threat alert after added threat base on user country__
- __Security Management authorization (for the moment everylogin user can access all menu) __
- __Approve Point of Area (need more information) __


Installation
--------------------
###Requirement 
- PHP >= 5.3.7 (test on 5.3 and 5.4)
- MCrypt PHP Extension
- PostgreSQL 9.*
- SQLite 3
- JQWidgets [Link](http://www.jqwidgets.com) [license](http://www.jqwidgets.com/license/)
- Laravel 4.1.* [Link](http://laravel.com/) [Document](http://laravel.com/docs/introduction)

Setup
-------------------
    This set up below will set up database and server key
    Database Configuration app/config/database.php
    'default' => 'pgsql',
    'pgsql' => array(
            'driver'   => 'pgsql',
            'host'     => 'localhost',
            'database' => 'database',
            'username' => '',#change username
            'password' => '',#change password
            'charset'  => 'utf8',
            'prefix'   => 'watchkeeper_',
            'schema'   => 'public',
    ),

    php artisan key:generate
    php artisan migrate:refresh
    php artisan migrate:install
    php artisan db:seed

Fixing permissions
-------------------
###This is really quite a easy fix
    chgrp -R www-data /var/www/laravel
    chmod -R 775 /var/www/laravel/app/storage


User
--------------------
- user "admin"
- password "abc12345"