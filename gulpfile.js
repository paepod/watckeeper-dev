var gulp = require('gulp');
var watch = require('gulp-watch');
var phpunit = require('gulp-phpunit');
var notify = require('gulp-notify');
var growl = require('gulp-notify-growl');
var growlNotifier = growl();
/* PHPUnit */
gulp.task('phpunit', function() {
    //notify defaults to false. If you don't want to use a notifier or worry with errors in this task leave it off
    var options = {debug: false, notify: true}
    gulp.src('app/tests/*.php')
        .pipe(phpunit('.\\vendor\\bin\\phpunit', options)) //empty phpunit path defaults ./vendor/bin/phpunit for windows specify with double back slashes

        //both notify and notify.onError will take optional notifier: growlNotifier for windows notifications
        //if options.notify is true be sure to handle the error here or suffer the consequenses!
        .on('error', notify.onError({
            title: 'PHPUnit Failed',
            message: 'One or more tests failed, see the cli for details.',
            notifier: growlNotifier
        }))

        //will fire only if no error is caught
        .pipe(notify({
            title: 'PHPUnit Passed',
            message: 'All tests passed!',
            notifier: growlNotifier
        }));
});

gulp.task('watch', function() {
    gulp.watch('app/**/*.php', ['phpunit']);
});

gulp.task('default',['phpunit','watch']);
