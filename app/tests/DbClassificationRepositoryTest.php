<?php
use Mockery as m;
use Way\Tests\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model\Eloquent as Eloquent;
use Immap\Watchkeeper\Classification as Classification;
use Immap\Watchkeeper\Services\Validators\ValidationException as ValidationException;
use Immap\Watchkeeper\Repositories\DbClassificationRepository as DbClassificationRepository;
use Illuminate\Support\Facades\Event as Event;
class DbClassificationRepositoryTest extends TestCase {

    public function __construct()
    {

    }

    public function setUp()
    {
        parent::setUp();
        $this->mockClassification = m::mock('lluminate\Database\Eloquent\Model\Eloquent','Immap\Watchkeeper\Classification');
        $this->mockClassificationRepo = m::mock('Immap\Watchkeeper\Repositories\Interfaces\ClassificationRepositoryInterface');
        $this->translator = m::mock('Symfony\Component\Translation\TranslatorInterface');
        $this->validator = m::mock('Illuminate\Validation\Factory');
        $this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
    }

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }
    public function testGetAllClassificationTypeIsNotNull()
    {
        $this->mockClassificationRepo->shouldReceive("getAllClassificationType")
                                     ->once()
                                     ->andReturn(array());
        $this->assertNotNull($this->mockClassificationRepo->getAllClassificationType());
    }

    public function testGetClassificationByIdWith1()
    {
        $this->mockClassificationRepo->shouldReceive("getClassificationTypeById")
                                     ->with(1)
                                     ->once()
                                     ->andReturn("incident type");
        $expect = "incident type";
        $this->assertEquals($expect,$this->mockClassificationRepo->getClassificationTypeById(1));
    }

    public function testCreateClassification()
    {
        $data['code'] = "c012";
        $data['name'] = "classification_type02";
        $data['group_id'] = "1";
        $expect = true;
        $this->classification = new Classification();
        $this->classificationRepo = new DbClassificationRepository($this->classification);
        Event::shouldReceive("fire")->once()->with('classification.saving',array($data));
        $result = $this->classificationRepo->create($data);
        $this->assertEquals($expect,$result);
    }
    public function testUpdateClassification()
    {
        $data['id'] = 1;
        $data['code'] = "c011";
        $data['name'] = "classification_type01";
        $data['group_id'] = "1";
        $expect = true;
        $this->classification = $this->prepareForUpdate();
        $this->classificationRepo = new DbClassificationRepository($this->classification);
        Event::shouldReceive("fire")->once()->with('classification.saving',array($data));
        $result = $this->classificationRepo->update($data);
        $this->assertTrue($result);

        $expectClassification = $this->classificationRepo->byId($data['id'])->first();
        $this->assertEquals($expectClassification->code,$data['code']);
        $this->assertEquals($expectClassification->name,$data['name']);
        $this->assertEquals($expectClassification->group_id,$data['group_id']);

    }

    public function prepareForUpdate()
    {
        $this->classification = new Classification();
        $this->classification->id = 1;
        $this->classification->code = "austin";
        $this->classification->name = "power";
        $this->classification->group_id = "1";
        $this->classification->save();
        return $this->classification;
    }

    /**
     * @expectedException Immap\Watchkeeper\Services\Validators\ValidationException
     */
    public function testValidationExceptionOnCreate()
    {
        $data['code'] = "1";
        $data['name'] = "classification";
        $data['group_id'] = "1";
        $this->mockClassificationRepo->shouldReceive("create")->once()->with($data)->andThrow(new ValidationException($this->validator));
        $this->mockClassificationRepo->create($data);
    }

    /**
     * @expectedException Immap\Watchkeeper\Services\Validators\ValidationException
     */
    public function testValidationExceptionOnUpdate()
    {
        $data['code'] = "1";
        $data['name'] = "classification";
        $data['group_id'] = "1";
        $this->mockClassificationRepo->shouldReceive("update")
                                     ->once()->with($data)
                                     ->andThrow(new ValidationException($this->validator));
        $this->mockClassificationRepo->update($data);
    }





}
