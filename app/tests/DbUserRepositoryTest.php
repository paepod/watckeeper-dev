<?php
use Mockery as m;
use Way\Tests\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model\Eloquent as Eloquent;
use Immap\Watchkeeper\Repositories\DbUserRepository as DbUserRepository;
use Immap\Watchkeeper\User as User;
use Immap\Watchkeeper\Role as Role;
use Illuminate\Support\Facades\Event as Event;
use Illuminate\Support\Facades\Crypt as Crypt;
class DbUserRepositoryTest extends TestCase {

    public function __construct()
    {
    }

    public function setUp()
    {
        parent::setUp();
        $this->mockUser = m::mock('lluminate\Database\Eloquent\Model\Eloquent','Immap\Watchkeeper\User');
        $this->mockTranslator = m::mock('Symfony\Component\Translation\TranslatorInterface');
        $this->mockUserRepo = m::mock('Immap\Watchkeeper\Repositories\Interfaces\UserRepositoryInterface','Immap\Watchkeeper\Repositories\DbUserRepository');
        $this->mockValidator = m::mock('Illuminate\Validation\Factory');
        $this->mockCollection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
        $this->user = new User();
        $this->rawPassword = "";
        $this->data = array();
        $this->userRepo = new DbUserRepository($this->user);
    }

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    public function test_make_password_default_length_5()
    {
        $this->assertEquals(5,mb_strlen($this->userRepo->makePassword()));
    }

    public function test_create_user()
    {
        $this->prepareCreateData();
        Event::shouldReceive('fire')->once()->with('user.saving',array($this->data))->ordered();
        Event::shouldReceive('fire')->once()->with('user.created', m::any() )->ordered();
        $result = $this->userRepo->create($this->data);
        $this->assertTrue($result);
        $user = $this->userRepo->byId(1);
        $this->assertEquals($user->username,$this->data['username']);
        $this->assertEquals($user->email,$this->data['email']);
    }

    public function test_encrypt_and_decrypt_username()
    {
        $username = 'AAAAA';
        $encryptUsername = $this->userRepo->encryptUsername($username);
        $result = $this->userRepo->decryptUsername($encryptUsername);
        $this->assertEquals($username,$result);
    }

    public function test_encrypt_and_decrypt_email()
    {
        $email = 'aaa@aaa.com';
        $encryptEmail = $this->userRepo->encryptEmail($email);
        $result = $this->userRepo->decryptEmail($encryptEmail);
        $this->assertEquals($email,$result);
    }

    public function test_attach_roles()
    {
        $faker = Faker\Factory::create();
        $data['firstname'] = $faker->firstName;
        $data['lastname'] = $faker->lastName;
        $data['middlename'] = $faker->firstName;
        $data['username'] = $faker->userName;
        $data['email'] = $faker->email;
        $data['status'] = true;
        $this->userRepo->create($data);

        $administrator = new Role();
        $administrator->name = 'administrator';
        $administrator->display_name = 'Administrator';
        $administrator->save();

        $superUser = new Role();
        $superUser->name = 'super_user';
        $superUser->display_name = 'Super User';
        $superUser->save();
        $user = $this->userRepo->getModel()->where('lastname',$data['lastname'])->first();
        $this->userRepo->attachRoles($user->id, array($administrator->id,$superUser->id));
        $this->assertEquals(true,$user->roles->contains($administrator->id));
        $this->assertEquals(true,$user->roles->contains($superUser->id));
    }

    public function test_detach_roles()
    {
        $faker = Faker\Factory::create();
        $data['firstname'] = $faker->firstName;
        $data['lastname'] = $faker->lastName;
        $data['middlename'] = $faker->firstName;
        $data['username'] = $faker->userName;
        $data['email'] = $faker->email;
        $this->userRepo->create($data);

        $administrator = new Role();
        $administrator->name = 'administrator';
        $administrator->display_name = 'Administrator';
        $administrator->save();

        $superUser = new Role();
        $superUser->name = 'super_user';
        $superUser->display_name = 'Super User';
        $superUser->save();
        $user = $this->userRepo->getModel()->where('lastname',$data['lastname'])->first();
        $this->userRepo->attachRoles($user->id, array($administrator->id,$superUser->id));
        $this->userRepo->attachRoles($user->id, array($administrator->id,$superUser->id));
        $this->userRepo->detachRoles($user->id, $superUser->id);

        $this->assertEquals(false,$user->roles->contains($superUser->id));

    }

    public function prepareCreateData()
    {
        $faker = Faker\Factory::create();
        $this->data['firstname'] = $faker->firstName;
        $this->data['lastname'] = $faker->lastName;
        $this->data['middlename'] = $faker->firstName;
        $this->data['username'] = $faker->userName;
        $this->data['email'] = $faker->email;

        $this->user->username = $this->data['username'];
        $this->user->email = $this->data['email'];
        $this->rawPassword = $this->userRepo->makePassword();
        $this->user->password = $this->rawPassword;
        $this->user->firstname = $this->data['firstname'];
        $this->user->lastname = $this->data['lastname'];
        $this->user->middlename = $this->data['middlename'];
    }
}
