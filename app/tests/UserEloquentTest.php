<?php
use Illuminate\Database\Eloquent\Model\Eloquent as Eloquent;
use Immap\Watchkeeper\User as User;
class UserEloquentyTest extends TestCase {

    public function setUp()
    {
        parent::setUp();
        $this->user = new User();
    }

    public function test_get_fullName()
    {
        $this->user->firstname = "first";
        $this->user->middletname = "middle";
        $this->user->lastnamename = "lastname";
        $expects = $this->user->firstname . ' ' . $this->user->middlename . ' ' . $this->user->lastname;
        $this->assertEquals($expects, $this->user->getFullName());
    }
}
