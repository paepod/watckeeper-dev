<?php
$I = new TestGuy($scenario);
$I->wantTo("Read Create Update Delete Permissions");
$I->amOnPage('/admin/permissions');
$I->see('List of Permissions','h1');
$I->amGoingTo('Create Permissions');
$I->amOnPage('admin/permissions');
$I->click('Add New');
$I->seeCurrentUrlEquals('/admin/permissions/create');
$I->amGoingTo('fill and submit the permissions form');
$name = 'test_permissions_test_'.(int)microtime(true);
$I->fillField('id-field-name',$name);
$I->fillField('id-field-display_name',$name);
$I->fillField('id-field-group_name','Administrator');
$I->click('Create');
$I->seeCurrentUrlEquals('/admin/permissions');
$nameUD = $name."UD";
$I->amGoingTo("Edit Permission (from $name to $nameUD)");
$I->amGoingTo('back to Permissions index pages');
$I->click('Manage Permissions');
$I->seeCurrentUrlEquals('/admin/permissions');
$I->click("edit_$name");
$I->seeCurrentUrlMatches('~/admin/permissions/(\d+)/edit~');
$I->fillField('id-field-name',$nameUD);
$I->fillField('id-field-display_name',$nameUD);
$I->click('Update');
$I->seeCurrentUrlEquals('/admin/permissions');
$I->dontSeeLink("edit_$name");
$I->seeLink("delete_$nameUD");
$I->amGoingTo('delete');
//$I->seeCurrentUrlEquals('/admin/permissions');
$I->click("delete_$nameUD");
$I->dontSeeLink("delete_$nameUD");


