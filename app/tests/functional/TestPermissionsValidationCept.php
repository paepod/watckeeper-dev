<?php
$I = new TestGuy($scenario);
$I->wantTo('validate permissions by name and display name');
$I->amOnPage('admin/permissions');
$I->click('Add New');
$I->seeCurrentUrlEquals('/admin/permissions/create');
$I->fillField('id-field-name','');
$I->fillField('id-field-display_name','');
$I->fillField('id-field-group_name','Administrator');
$I->click('Create');
$I->see('The name field is required.','div');
$I->see('The display name field is required.','div');
$I->amGoingTo('fill and submit the permissions form with manage_countries and Manage Countries');
$I->fillField('id-field-name','manage_countries');
$I->fillField('id-field-display_name','Manage Countries');
$I->fillField('id-field-group_name','Administrator');
$I->click('Create');
$I->see('The name has already been taken.','div');
$I->see('The display name has already been taken','div');


